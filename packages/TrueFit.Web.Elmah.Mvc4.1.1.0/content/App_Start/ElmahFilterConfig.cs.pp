﻿using System.Web.Mvc;
using TrueFit.Web.Elmah;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof ($rootnamespace$.App_Start.ElmahFilterConfig), "PreStart")]

namespace $rootnamespace$.App_Start
{
  public static class ElmahFilterConfig
  {
    public static void PreStart()
    {
      GlobalFilters.Filters.Add(new ElmahExceptionFilter());
    }
  }
}
