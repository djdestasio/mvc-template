﻿using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using StructureMap;
using TrueFit.Web.StructureMap;
using TrueFit.Web.StructureMap.Validation;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof ($rootnamespace$.App_Start.StructureMapConfig), "PreStart")]

namespace $rootnamespace$.App_Start
{
  public static class StructureMapConfig
  {
    public static void PreStart()
    {
      DynamicModuleUtility.RegisterModule(typeof (ReleaseHttpScopedObjectsModule));
      InitializeObjectFactory();
      DependencyResolver.SetResolver(new StructureMapDependencyResolver(ObjectFactory.Container));
	  GlobalConfiguration.Configuration.DependencyResolver = new StructureMapApiDependencyResolver(ObjectFactory.Container);
    }

    private static void InitializeObjectFactory()
    {
      ObjectFactory.Initialize(
        c =>
          {
            c.Scan(s =>
                     {
                       //TODO: Add calls to scan additional assemblies in your solution that contain StructureMap Registries
                       //Example: s.AssemblyContainingType<IQueryExecutor>();
                       s.AssemblyContainingType<StructureMapDependencyResolver>();
                       s.TheCallingAssembly();
                       s.AddAllTypesOf(typeof(IModelValidator<>));
                       s.LookForRegistries();
                     });
            c.SetAllProperties(sc => sc.OfType<IContainer>());
          });
    }
  }
}
