Registers a Global Filter with ASP.NET MVC in order to catch exceptions that
come through the pipeline as handled and make sure they still get logged to
Elmah.

If you catch an exception in your action and want to ensure that the exception
is logged to Elmah, you can call the static LogHandledException on
ElmahExceptionFilter.
