﻿using System.Web.Mvc;
using TrueFit.Web.Elmah;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof ($rootnamespace$.App_Start.AutoMapperConfiguration), "PreStart")]

namespace $rootnamespace$.App_Start
{
  public static class ElmahFilterRegistration
  {
    public static void PreStart()
    {
      GlobalFilters.Filters.Add(new ElmahExceptionFilter());
    }
  }
}
