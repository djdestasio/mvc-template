ENV['server'] = "(local)" if !ENV['server']
ENV['database'] = "" if !ENV['database']

#MIGRATE_COMMAND = "src/packages/TrueFit.Schema.1.3.0/tools/tfschemasql.exe --server #{ENV['server']} --database #{ENV['database']} --schemaFilePath src/MyProject.Domain/Schema/Schema.xml"

namespace :db do
	desc "Creates an empty database, installs the catalog tables and migrates it to the latest version"
	task :create => [:create_empty_database, :create_catalog_tables, :migrate]
	  
  desc "Migrates the database to the latest version"
  task :migrate do
    sh MIGRATE_COMMAND
  end

  desc "Migrates the database to the newest version using the dryRun option to not commit any changes"
  task :test_migrate do
    sh MIGRATE_COMMAND + " --dryRun"
  end

  desc "Creates the schema catalog tables in the database"
  task :create_catalog_tables do
    sh MIGRATE_COMMAND + " --createCatalogTables"
  end

  desc "Drops the schema catalog tables in the database"
  task :drop_catalog_tables do
    sh MIGRATE_COMMAND + " --dropCatalogTables"
  end

  desc "Creates the catalog tables in the database necessary for the schema tooling to run"
  task :populate_catalog_tables do
    sh MIGRATE_COMMAND + " --catalogStmtsOnly"
  end
  
  desc "Creates an empty database"
  task :create_empty do
    sh "sqlcmd -X -S #{ENV["server"]} -E -Q \"CREATE DATABASE #{ENV["database"]}\""
  end
	
  desc "Drops the database"
  task :drop do
    sh "sqlcmd -X -S #{ENV["server"]} -E -Q \"DROP DATABASE #{ENV["database"]}\""
  end  
end
