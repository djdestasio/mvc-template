CREATE TABLE [TrueFitSchema] (
  [Name]     NVARCHAR(128) NOT NULL,
  [Version]  NVARCHAR(20) NOT NULL
)
GO
ALTER TABLE [TrueFitSchema] ADD CONSTRAINT [PK_TrueFitSchema] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaTable] (
  [Name]  NVARCHAR(128) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaTable] ADD CONSTRAINT [PK_TrueFitSchemaTable] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaTableColumn] (
  [TableName]          NVARCHAR(128) NOT NULL,
  [Name]               NVARCHAR(128) NOT NULL,
  [Type]               NVARCHAR(16) NOT NULL,
  [Size]               INT NULL,
  [Precision]          INT NULL,
  [Scale]              INT NULL,
  [AllowNull]          NVARCHAR(5) NOT NULL,
  [Identity]           NVARCHAR(5) NOT NULL,
  [ComputedExpression] NVARCHAR(MAX) NULL,
  [Persisted]          NVARCHAR(5) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaTableColumn] ADD CONSTRAINT [PK_TrueFitSchemaTableColumn] PRIMARY KEY ([TableName], [Name])
GO

CREATE TABLE [TrueFitSchemaComputedTableColumnReference] (
  [TableName]             NVARCHAR(128) NOT NULL,
  [ComputedColumnName]    NVARCHAR(128) NOT NULL,
  [ReferencedColumnName]  NVARCHAR(128) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaComputedTableColumnReference] ADD CONSTRAINT [PK_TrueFitSchemaComputedTableColumnReference] PRIMARY KEY ([TableName], [ComputedColumnName], [ReferencedColumnName])
GO

CREATE TABLE [TrueFitSchemaTableColumnDefault] (
  [TableName]           NVARCHAR(128) NOT NULL,
  [ColumnName]          NVARCHAR(128) NOT NULL,
  [Name]                NVARCHAR(128) NOT NULL,
  [ConstantExpression]  NVARCHAR(128) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaTableColumnDefault] ADD CONSTRAINT [PK_TrueFitSchemaTableColumnDefault] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaPrimaryKey] (
  [TableName]  NVARCHAR(128) NOT NULL,
  [Name]       NVARCHAR(128) NOT NULL,
  [Clustered]  NVARCHAR(5) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaPrimaryKey] ADD CONSTRAINT [PK_TrueFitSchemaPrimaryKey] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaPrimaryKeyColumn] (
  [PrimaryKeyName]  NVARCHAR(128) NOT NULL,
  [Name]            NVARCHAR(128) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaPrimaryKeyColumn] ADD CONSTRAINT [PK_TrueFitSchemaPrimaryKeyColumn] PRIMARY KEY ([PrimaryKeyName], [Name])
GO

CREATE TABLE [TrueFitSchemaForeignKey] (
  [TableName]            NVARCHAR(128) NOT NULL,
  [Name]                 NVARCHAR(128) NOT NULL,
  [ReferencesTableName]  NVARCHAR(128) NOT NULL,
  [OnUpdate]             NVARCHAR(10) NOT NULL,
  [OnDelete]             NVARCHAR(10) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaForeignKey] ADD CONSTRAINT [PK_TrueFitSchemaForeignKey] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaForeignKeyColumn] (
  [ForeignKeyName]  NVARCHAR(128) NOT NULL,
  [Name]            NVARCHAR(128) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaForeignKeyColumn] ADD CONSTRAINT [PK_TrueFitSchemaForeignKeyColumn] PRIMARY KEY ([ForeignKeyName], [Name])
GO

CREATE TABLE [TrueFitSchemaIndex] (
  [TableName]  NVARCHAR(128) NOT NULL,
  [Name]       NVARCHAR(128) NOT NULL,
  [Unique]     NVARCHAR(5) NOT NULL,
  [Clustered]  NVARCHAR(5) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaIndex] ADD CONSTRAINT [PK_TrueFitSchemaIndex] PRIMARY KEY ([TableName], [Name])
GO

CREATE TABLE [TrueFitSchemaIndexColumn] (
  [TableName]   NVARCHAR(128) NOT NULL,
  [IndexName]   NVARCHAR(128) NOT NULL,
  [Name]        NVARCHAR(128) NOT NULL,
  [ColumnType]  NVARCHAR(7) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaIndexColumn] ADD CONSTRAINT [PK_TrueFitSchemaIndexColumn] PRIMARY KEY ([TableName], [IndexName], [Name])
GO

CREATE TABLE [TrueFitSchemaView] (
  [Name]       NVARCHAR(128) NOT NULL,
  [CreateSql]  NVARCHAR(MAX) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaView] ADD CONSTRAINT [PK_TrueFitSchemaView] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaStoredProcedure] (
  [Name]       NVARCHAR(128) NOT NULL,
  [CreateSql]  NVARCHAR(MAX) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaStoredProcedure] ADD CONSTRAINT [PK_TrueFitSchemaStoredProcedure] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaUserDefinedFunction] (
  [Name]       NVARCHAR(128) NOT NULL,
  [CreateSql]  NVARCHAR(MAX) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaUserDefinedFunction] ADD CONSTRAINT [PK_TrueFitSchemaUserDefinedFunction] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaTrigger] (
  [Name]       NVARCHAR(128) NOT NULL,
  [CreateSql]  NVARCHAR(MAX) NOT NULL
)
GO
ALTER TABLE [TrueFitSchemaTrigger] ADD CONSTRAINT [PK_TrueFitSchemaTrigger] PRIMARY KEY ([Name])
GO

CREATE TABLE [TrueFitSchemaOneTimeScript] (
  [FileName]  NVARCHAR(259) NOT NULL,
  [RunDate]   DATETIME NOT NULL
)
ALTER TABLE [TrueFitSchemaOneTimeScript] ADD CONSTRAINT [PK_TrueFitSchemaOneTimeScript] PRIMARY KEY ([FileName])
GO