Support the DataTables jQuery grid plugin. http://datatables.net

Class Information:

DataTablesGridModel should be added to your action method that receives the
requests from your grid when performing all actions on the server.

DataTablesGridModelBinder is the ASP.NET MVC model binder for the
DataTablesGridModel

DataTablesGridData is a class that when serialized to JSON will contain the
aaData property that DataTables expects the server to return by default.

DataTablesServerSideGridData contains additional data that DataTables needs when
performing all actions on the server.

Extension Method Information:

The extension methods are meant to be used in the scenario when you want to use
the 2 dimensional array style of data source to feed the grid. This is the
default setup and often good enough, but more advanced bindings are available
and may be appropriate which would not require use of these extensions at all.

BuildGridData is an extension of of IEnumerable<T>. It's purpose is to assist
with taking a list of objects and turning it into an object (DataTablesGridData
or DataTablesServerSideGridData) that when serialized to JSON will contain the
data for the grid formatted in the needed way.

Both overloads take a list of delegates that when invoked provide the value to
put into a particular cell in a particular row of the grid. The delegates are
assumed to be in the same order as the cells in the grid.

Both overloads also take an optional delege to return the id to be applied to a
row and a delegate to return the class to be applied to a row.

One overload takes the total number of records available and total number of
records being displayed in the grid that can be used when performing paging on
the server.

BuildGridCellValueExpressions is the other extension method and is also an
extension of IEnumerable<T>. It allows you to easily build up the list of
delegates to pass to BuildGridData. All it does it use the params keyword to
easily return an array. No changes are made to your data.

Example:

In this example, my grid has two columns. And I have a list of ints I want to
transform for output. I would take the resulting gridData variable and serialize
it to JSON for my return value.

var myData = new int[] {1,2,3,4,5,6,7,8,9,10};
var exprs = myData.BuildGridCellValueExpressions(
  x => "The number is " + x.ToString(),
  x => "The number in hex is " + x.ToString("x"));
var gridData = myData.BuildGridData(exprs);
