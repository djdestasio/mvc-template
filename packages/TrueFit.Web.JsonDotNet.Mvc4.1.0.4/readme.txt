Replaces JSON serialization/deserialization with Json.Net instead of the built
in System.Web.Script.Serialization.JavaScriptSerializer. The main reason for
doing so is performance. JSON.NET has been shown to be considerably fast in
benchmarks. It also provides some more control and options for serializing
things like dates and dynamic objects.

For serialization you want to use the JsonDotNetResult. You can new one up and
return it, add a custom method like Json or View in your controller, or you can
override the existing Json method as follows to use it by default.

protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
{
  return new JsonDotNetResult
           {
             Data = data,
             ContentType = contentType,
             ContentEncoding = contentEncoding,
             JsonRequestBehavior = behavior
           };
}

For deserialization you use the JsonDotNetValueProviderFactory class.
You register it with the ASP.NET MVC application structure in one of two ways.

If you have configured an IDependencyResolver, simply set up your backing IoC
container to return an instance of JsonDotNetValueProviderFactory when asked
for a ValueProviderFactory.

If you are using the TrueFit Web StructureMap package, in the
StructureMapConfiguration class tell StructureMap to scan the
TrueFit.Web.JsonDotNet assembly as there is a registry provided to make the
registration for you.

Example:
s.AssemblyContainingType<JsonDotNetValueProviderFactory>();

If you wish to register the setup yourself, then you can follow this example:
For<ValueProviderFactory>().Use<JsonDotNetValueProviderFactory>().Named("JsonDotNetValueProviderFactory");

If you have not configured an IDependencyResolver, add an instance of
JsonDotNetValueProviderFactory to the static global collection as follows.

System.Web.Mvc.ValueProviderFactories.Factories.Add(new JsonDotNetValueProviderFactory());
