﻿using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using StructureMap;
using TrueFit.Web.StructureMap;
using TrueFit.Web.StructureMap.Validation;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof ($rootnamespace$.App_Start.StructureMapConfiguration), "PreStart")]

namespace $rootnamespace$.App_Start
{
  public static class StructureMapConfiguration
  {
    public static void PreStart()
    {
      DynamicModuleUtility.RegisterModule(typeof (ReleaseHttpScopedObjectsModule));
      InitializeObjectFactory();
      DependencyResolver.SetResolver(new StructureMapDependencyResolver(ObjectFactory.Container));
    }

    private static void InitializeObjectFactory()
    {
      ObjectFactory.Initialize(
        c =>
          {
            c.Scan(s =>
                     {
                       //TODO: Add calls to scan additional assemblies in your solution that contain StructureMap Registries
                       //Example: s.AssemblyContainingType<IQueryExecutor>();
                       s.AssemblyContainingType<StructureMapDependencyResolver>();
                       s.TheCallingAssembly();
                       s.AddAllTypesOf(typeof(IModelValidator<>));
                       s.LookForRegistries();
                     });
            c.SetAllProperties(sc => sc.OfType<IContainer>());
          });
    }
  }
}
