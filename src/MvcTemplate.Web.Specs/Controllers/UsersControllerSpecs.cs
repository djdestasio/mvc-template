﻿#region usings

using System.Linq;
using System.Web.Mvc;
using Moq;
using MvcTemplate.Domain.Entities;
using MvcTemplate.Domain.Queries;
using MvcTemplate.Domain.Queries.Users;
using MvcTemplate.Web.Controllers;
using NUnit.Framework;
using Shouldly;
using TrueFit.Web.DataTables;

#endregion

namespace MvcTemplate.Web.Specs.Controllers
{
  public class UsersControllerSpecs
  {
    [TestFixture]
    public class WhenRetrievingAListOfUsers
    {
      [Test]
      public void ItLoadsAllUsers()
      {
        var queryExecutor = new Mock<IQueryExecutor>();
        queryExecutor.Setup(qe => qe.ExecuteQuery(It.IsAny<AllUsers>())).Returns(Enumerable.Empty<User>());

        var sut = new UsersController(null) {QueryExecutor = queryExecutor.Object};

        sut.ListData();

        queryExecutor.Verify(qe => qe.ExecuteQuery(It.IsAny<AllUsers>()));
      }

      [Test]
      public void ItReturnsDataTablesGridData()
      {
        var queryExecutor = new Mock<IQueryExecutor>();
        queryExecutor.Setup(qe => qe.ExecuteQuery(It.IsAny<AllUsers>())).Returns(Enumerable.Empty<User>());

        var sut = new UsersController(null) {QueryExecutor = queryExecutor.Object};

        var result = sut.ListData();

        result.Data.ShouldBeTypeOf<DataTablesGridData>();
      }

      [Test]
      public void ItSerializesTheReturnDataToJson()
      {
        var queryExecutor = new Mock<IQueryExecutor>();
        queryExecutor.Setup(qe => qe.ExecuteQuery(It.IsAny<AllUsers>())).Returns(Enumerable.Empty<User>());

        var sut = new UsersController(null) {QueryExecutor = queryExecutor.Object};

        var result = sut.ListData();

        result.ShouldBeTypeOf<JsonResult>();
      }
    }
  }
}