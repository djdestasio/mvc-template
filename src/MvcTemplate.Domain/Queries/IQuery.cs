#region usings

using NHibernate;

#endregion

namespace MvcTemplate.Domain.Queries
{
  public interface IQuery<out TReturn>
  {
    TReturn Execute(ISession session);
  }
}