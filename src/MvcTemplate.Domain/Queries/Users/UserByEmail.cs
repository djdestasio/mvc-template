﻿#region usings

using MvcTemplate.Domain.Entities;
using NHibernate;

#endregion

namespace MvcTemplate.Domain.Queries.Users
{
  public class UserByEmail : IQuery<User>
  {
    private readonly string _email;

    public UserByEmail(string email)
    {
      _email = email;
    }

    public User Execute(ISession session)
    {
      return session.QueryOver<User>().Where(u => u.Email == _email).SingleOrDefault();
    }
  }
}