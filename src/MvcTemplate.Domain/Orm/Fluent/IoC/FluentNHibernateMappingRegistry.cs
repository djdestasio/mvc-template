﻿#region usings

using NHibernate;
using StructureMap.Configuration.DSL;

#endregion

namespace MvcTemplate.Domain.Orm.Fluent.IoC
{
  public class FluentNHibernateMappingRegistry : Registry
  {
    public FluentNHibernateMappingRegistry()
    {
      For<ISessionFactoryBuilder>().Singleton().Use<SessionFactoryBuilder>();
      For<ISessionFactory>().Singleton().Use(context => context.GetInstance<ISessionFactoryBuilder>().BuildISessionFactory());
    }
  }
}