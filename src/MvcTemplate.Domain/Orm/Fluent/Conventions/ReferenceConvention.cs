#region usings

using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

#endregion

namespace MvcTemplate.Domain.Orm.Fluent.Conventions
{
  public class ReferenceConvention : IReferenceConvention
  {
    public void Apply(IManyToOneInstance instance)
    {
      instance.Column(instance.Property.PropertyType.Name + "Id");
    }
  }
}