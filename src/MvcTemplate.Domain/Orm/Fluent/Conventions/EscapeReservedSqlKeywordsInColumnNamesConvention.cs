#region usings

using System;
using System.Collections.Generic;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;

#endregion

namespace MvcTemplate.Domain.Orm.Fluent.Conventions
{
  public class EscapeReservedSqlKeywordsInColumnNamesConvention : IPropertyConvention, IPropertyConventionAcceptance
  {
    private readonly HashSet<string> _keywords =
      new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
          "ADD", "EXISTS", "PRECISION", "ALL", "EXIT", "PRIMARY", "ALTER", "EXTERNAL", "PRINT", "AND", "FETCH", "PROC", "ANY", "FILE", "PROCEDURE", "AS", "FILLFACTOR", "PUBLIC", "ASC", "FOR", "RAISERROR", "AUTHORIZATION", "FOREIGN",
          "READ", "BACKUP", "FREETEXT", "READTEXT", "BEGIN", "FREETEXTTABLE", "RECONFIGURE", "BETWEEN", "FROM", "REFERENCES", "BREAK", "FULL", "REPLICATION", "BROWSE", "FUNCTION", "RESTORE", "BULK", "GOTO", "RESTRICT",
          "BY", "GRANT", "RETURN", "CASCADE", "GROUP", "REVERT", "CASE", "HAVING", "REVOKE", "CHECK", "HOLDLOCK", "RIGHT", "CHECKPOINT", "IDENTITY", "ROLLBACK", "CLOSE", "IDENTITY_INSERT", "ROWCOUNT", "CLUSTERED", "IDENTITYCOL",
          "ROWGUIDCOL", "COALESCE", "IF", "RULE", "COLLATE", "IN", "SAVE", "COLUMN", "INDEX", "SCHEMA", "COMMIT", "INNER", "SECURITYAUDIT", "COMPUTE", "INSERT", "SELECT", "CONSTRAINT", "INTERSECT", "SESSION_USER", "CONTAINS", "INTO",
          "SET", "CONTAINSTABLE", "IS", "SETUSER", "CONTINUE", "JOIN", "SHUTDOWN", "CONVERT", "KEY", "SOME", "CREATE", "KILL", "STATISTICS", "CROSS", "LEFT", "SYSTEM_USER", "CURRENT", "LIKE", "TABLE", "CURRENT_DATE", "LINENO", "TABLESAMPLE",
          "CURRENT_TIME", "LOAD", "TEXTSIZE", "CURRENT_TIMESTAMP", "MERGE", "THEN", "CURRENT_USER", "NATIONAL", "TO", "CURSOR", "NOCHECK", "TOP", "DATABASE", "NONCLUSTERED", "TRAN", "DBCC", "NOT", "TRANSACTION", "DEALLOCATE", "NULL",
          "TRIGGER", "DECLARE", "NULLIF", "TRUNCATE", "DEFAULT", "OF", "TSEQUAL", "DELETE", "OFF", "UNION", "DENY", "OFFSETS", "UNIQUE", "DESC", "ON", "UNPIVOT", "DISK", "OPEN", "UPDATE", "DISTINCT", "OPENDATASOURCE", "UPDATETEXT",
          "DISTRIBUTED", "OPENQUERY", "USE", "DOUBLE", "OPENROWSET", "USER", "DROP", "OPENXML", "VALUES", "DUMP", "OPTION", "VARYING", "ELSE", "OR", "VIEW", "END", "ORDER", "WAITFOR", "ERRLVL", "OUTER", "WHEN", "ESCAPE", "OVER", "WHERE", "EXCEPT",
          "PERCENT", "WHILE", "EXEC", "PIVOT", "WITH", "EXECUTE", "PLAN", "WRITETEXT", "PERMISSIONS"
        };

    public void Apply(IPropertyInstance instance)
    {
      if (_keywords.Contains(instance.Name))
      {
        instance.Column(string.Format("[{0}]", instance.Name));
      }
    }

    public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
    {
    }
  }
}