﻿using FluentNHibernate.Mapping;
using MvcTemplate.Domain.Entities;

namespace MvcTemplate.Domain.Orm.Fluent.Mappings
{
  public class UserMap : ClassMap<User>
  {
    public UserMap()
    {
      Id(x => x.Id);
      Version(x => x.Version);
      Map(x => x.CreateDate);
      Map(x => x.Email);
      Map(x => x.Password);
      Map(x => x.Salt);
      Map(x => x.PasswordResetToken);
      Map(x => x.PasswordResetTokenExpirationDate);
    }
  }
}