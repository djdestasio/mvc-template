﻿#region usings

using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using MvcTemplate.Domain.Entities;
using MvcTemplate.Domain.Orm.Fluent.Conventions;
using NHibernate;


#endregion

namespace MvcTemplate.Domain.Orm.Fluent
{
  public class SessionFactoryBuilder : ISessionFactoryBuilder
  {
    private readonly IConnectionStringProvider _connectionStringProvider;

    public SessionFactoryBuilder(IConnectionStringProvider connectionStringProvider)
    {
      _connectionStringProvider = connectionStringProvider;
    }

    public ISessionFactory BuildISessionFactory()
    {
      return Fluently.Configure()
        .Database(MsSqlConfiguration.MsSql2008.ConnectionString(_connectionStringProvider.GetConnectionString()))
        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<User>()
                        .Conventions.AddFromAssemblyOf<TableNameConvention>())
        .BuildSessionFactory();
    }
  }
}