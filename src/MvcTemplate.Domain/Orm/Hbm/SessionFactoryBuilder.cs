﻿#region usings

using MvcTemplate.Domain.Entities;
using NHibernate;
using NHibernate.Bytecode;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using Configuration = NHibernate.Cfg.Configuration;

#endregion

namespace MvcTemplate.Domain.Orm.Hbm
{
  public class SessionFactoryBuilder : ISessionFactoryBuilder
  {
    private readonly IConnectionStringProvider _connectionStringProvider;

    public SessionFactoryBuilder(IConnectionStringProvider connectionStringProvider)
    {
      _connectionStringProvider = connectionStringProvider;
    }

    public ISessionFactory BuildISessionFactory()
    {
      var configuration = new Configuration()
        .Proxy(p => p.ProxyFactoryFactory<DefaultProxyFactoryFactory>())
        .DataBaseIntegration(
          d =>
            {
              d.ConnectionProvider<DriverConnectionProvider>();
              d.Dialect<MsSql2008Dialect>();
              d.BatchSize = 10;
              d.ConnectionString = _connectionStringProvider.GetConnectionString();
            })
        .AddAssembly(typeof (User).Assembly);
      return configuration.BuildSessionFactory();
    }
  }
}