﻿#region usings

using NHibernate;

#endregion

namespace MvcTemplate.Domain.Orm
{
  public interface ISessionFactoryBuilder
  {
    ISessionFactory BuildISessionFactory();
  }
}