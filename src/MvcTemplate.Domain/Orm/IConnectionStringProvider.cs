﻿namespace MvcTemplate.Domain.Orm
{
  public interface IConnectionStringProvider
  {
    string GetConnectionString();
  }
}