﻿using System.Web.Mvc;

namespace MvcTemplate.Web.Controllers
{
  public class SharedController : ControllerBase
  {
    [HttpGet]
    public PartialViewResult ConfirmDelete(string url, int id)
    {
      ViewBag.Url = url;
      ViewBag.Id = id;
      return PartialView("_ConfirmDelete");
    }
  }
}