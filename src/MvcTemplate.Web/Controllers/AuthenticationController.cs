﻿#region usings

using System.Web.Mvc;
using MvcTemplate.Web.Infrastructure.Authentication;
using MvcTemplate.Web.Models;

#endregion

namespace MvcTemplate.Web.Controllers
{
  public class AuthenticationController : ControllerBase
  {
    private readonly IAuthenticationService _authenticationService;

    public AuthenticationController(IAuthenticationService authenticationService)
    {
      _authenticationService = authenticationService;
    }

    [HttpGet]
    public ActionResult LogOn()
    {
      return View();
    }

    [HttpPost, ValidateAntiForgeryToken]
    public ActionResult LogOn(LogOnForm form, string returnUrl)
    {
      if (ModelState.IsValid)
      {
        if (_authenticationService.LogOn(form.Email, form.Password))
        {
          return Url.IsLocalUrl(returnUrl) ? (ActionResult) Redirect(returnUrl) : RedirectToAction("Index", "Home");
        }

        ModelState.AddModelError("", "The username or password provided is incorrect.");
      }

      return View(form);
    }

    [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Delete)]
    public ActionResult LogOff()
    {
      _authenticationService.LogOff();
      return RedirectToAction("Index", "Home");
    }
  }
}