﻿#region usings

using System.Text;
using System.Web.Mvc;
using MvcTemplate.Domain.Entities;
using MvcTemplate.Domain.Queries;
using MvcTemplate.Web.Infrastructure.Filters;
using NHibernate;
using TrueFit.Web.Elmah;
using TrueFit.Web.JsonDotNet;

#endregion

namespace MvcTemplate.Web.Controllers
{
  [NHibernateTransaction]
  public abstract class ControllerBase : Controller
  {
    private User _currentUser;

    // ReSharper disable MemberCanBeProtected.Global
    // ReSharper disable UnusedAutoPropertyAccessor.Global
    public ISession NhSession { get; set; }
    public IQueryExecutor QueryExecutor { get; set; }
    // ReSharper restore UnusedAutoPropertyAccessor.Global
    // ReSharper restore MemberCanBeProtected.Global

    protected int CurrentUserId { get { return int.Parse(User.Identity.Name); } }
    protected User CurrentUser { get { return _currentUser ?? (_currentUser = NhSession.Get<User>(CurrentUserId)); } }

    protected override void OnException(ExceptionContext exceptionContext)
    {
      if (exceptionContext.ExceptionHandled)
      {
        ElmahExceptionFilter.LogHandledException(exceptionContext.Exception);
      }

      if (NhSession.Transaction.IsActive)
      {
        NhSession.Transaction.Rollback();
      }

      base.OnException(exceptionContext);
    }

    protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
    {
      return new JsonDotNetResult
               {
                 Data = data,
                 ContentType = contentType,
                 ContentEncoding = contentEncoding,
                 JsonRequestBehavior = behavior
               };
    }
  }
}