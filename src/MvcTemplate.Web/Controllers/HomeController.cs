﻿#region usings

using System.Web.Mvc;

#endregion

namespace MvcTemplate.Web.Controllers
{
  public class HomeController : ControllerBase
  {
    [HttpGet]
    public ActionResult Index()
    {
      ViewBag.Message = "Welcome to MvcTemplate!";
      return View();
    }
  }
}