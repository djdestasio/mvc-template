﻿#region usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MvcTemplate.Domain.Entities;
using TrueFit.Web.DataTables;

#endregion

namespace MvcTemplate.Web.Models.GridBuilders
{
  public class UsersGridDataBuilder
  {
    public DataTablesGridData BuildGridData(IEnumerable recordModels)
    {
      var users = recordModels.OfType<User>().ToList();
      return users.BuildGridData(BuildTableCellExpressions(users));
    }

    private IEnumerable<Func<User, object>> BuildTableCellExpressions(IEnumerable<User> users)
    {
      return users.BuildGridCellValueExpressions(
        x => x.Email,
        x => "<a>Edit</a>",
        x => "<a>Delete</a>",
        x => x.Id);
    }
  }
}