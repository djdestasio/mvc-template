#region usings

using System.ComponentModel.DataAnnotations;

#endregion

namespace MvcTemplate.Web.Models
{
  public class LogOnForm
  {
    [Required]
    public string Email { get; set; }

    [Required, DataType(DataType.Password)]
    public string Password { get; set; }
  }
}