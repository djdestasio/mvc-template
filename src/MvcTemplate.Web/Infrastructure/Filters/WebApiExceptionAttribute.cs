﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using NHibernate;
using StructureMap;

namespace MediaEinstein.Web.Infrastructure.Filters
{
    public class WebApiExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var session = ObjectFactory.GetInstance<ISession>();

            if (session.Transaction.IsActive)
            {   
                session.Transaction.Rollback();
            }

            base.OnException(actionExecutedContext);
        }
    }
}