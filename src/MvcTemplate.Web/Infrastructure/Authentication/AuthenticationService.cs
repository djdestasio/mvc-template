﻿#region usings

using MvcTemplate.Domain;
using MvcTemplate.Domain.Queries;
using MvcTemplate.Domain.Queries.Users;

#endregion

namespace MvcTemplate.Web.Infrastructure.Authentication
{
  public class AuthenticationService : IAuthenticationService
  {
    private readonly IFormsAuthenticationService _formsAuthenticationService;
    private readonly IQueryExecutor _queryExecutor;
    private readonly PasswordHasher _passwordHasher;

    public AuthenticationService(IFormsAuthenticationService formsAuthenticationService, IQueryExecutor queryExecutor, PasswordHasher passwordHasher)
    {
      _formsAuthenticationService = formsAuthenticationService;
      _queryExecutor = queryExecutor;
      _passwordHasher = passwordHasher;
    }

    public bool LogOn(string email, string password)
    {
      var user = _queryExecutor.ExecuteQuery(new UserByEmail(email));
      var successful = user != null && user.Password == _passwordHasher.HashPassword(user.Salt, password);
      if (successful)
      {
        _formsAuthenticationService.LogOn(user.Id, false);
      }
      return successful;
    }

    public void LogOff()
    {
      _formsAuthenticationService.LogOff();
    }
  }
}