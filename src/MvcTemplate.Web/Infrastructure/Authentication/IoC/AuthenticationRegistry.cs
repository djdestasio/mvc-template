﻿#region usings

using StructureMap.Configuration.DSL;

#endregion

namespace MvcTemplate.Web.Infrastructure.Authentication.IoC
{
  public class AuthenticationRegistry : Registry
  {
    public AuthenticationRegistry()
    {
      For<IAuthenticationService>().Use<AuthenticationService>();
      For<IFormsAuthenticationService>().Use<FormsAuthenticationService>();

      SetAllProperties(sc =>
                         {
                           sc.OfType<IAuthenticationService>();
                           sc.OfType<IFormsAuthenticationService>();
                         });
    }
  }
}