﻿#region usings

using System.Collections.Generic;
using System.IO;
using System.Web.Optimization;
using MediaEinstein.Web.Infrastructure.BundleTransforms;

#endregion

namespace MediaEinstein.Web.App_Start
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            var bootstrap = new Bundle("~/bundles/bootstrap")
                .Include("~/Content/less/bootstrap.less");

            bootstrap.Transforms.Add(new LessTransform());
            bootstrap.Transforms.Add(new CssMinify());

            bundles.Add(bootstrap);

            var siteCssBundle =
                new Bundle("~/bundles/css/site", new CssMinify())
                    .Include("~/Content/Css/lib/bootstrap/bootstrap.overrides.css")
                    .Include("~/Content/Css/lib/bootstrap/datepicker.css")
                    .Include("~/Content/Css/lib/jquery/jgrowl.css")
                    .Include("~/Content/Css/form-validation.css")
                    .Include("~/Content/Css/site.css");
            siteCssBundle.Orderer = new InAddedOrder();
            bundles.Add(siteCssBundle);

            bundles.Add(
                new Bundle("~/bundles/js/require", new JsMinify())
                    .Include("~/Scripts/require.js"));
        }

        private class InAddedOrder : IBundleOrderer
        {
            public IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
            {
                return files;
            }
        }
    }
}