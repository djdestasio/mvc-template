#region usings

using System.Web.Mvc;
using MvcTemplate.Web.App_Start;
using TrueFit.Web.Elmah;
using WebActivator;

#endregion

[assembly: PreApplicationStartMethod(typeof (ElmahFilterConfig), "PreStart")]

namespace MvcTemplate.Web.App_Start
{
  public static class ElmahFilterConfig
  {
    public static void PreStart()
    {
      GlobalFilters.Filters.Add(new ElmahExceptionFilter());
    }
  }
}