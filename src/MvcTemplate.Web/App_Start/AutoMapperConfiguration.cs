using System;
using WebActivator;

[assembly: PreApplicationStartMethod(typeof (MvcTemplate.Web.App_Start.AutoMapperConfiguration), "PreStart")]

namespace MvcTemplate.Web.App_Start
{
  public static class AutoMapperConfiguration
  {
    public static void PreStart()
    {
      Configure(Activator.CreateInstance);
      // If you are using StructureMap uncomment this line and
      // remove the line above to get dependency injection in
      // AutoMapper support objects.
      //Configure(ObjectFactory.GetInstance);
    }

    private static void Configure(Func<Type, object> createDependency)
    {
      AutoMapper.Mapper.Initialize(
        cfg =>
          {
            cfg.ConstructServicesUsing(createDependency);
          });
    }
  }
}