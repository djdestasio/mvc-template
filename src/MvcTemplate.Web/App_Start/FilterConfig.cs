﻿#region usings

using System.Web;
using System.Web.Mvc;
using MediaEinstein.Web.App_Start;
using TrueFit.Web.Elmah;

#endregion

[assembly: WebActivator.PreApplicationStartMethod(typeof(FilterConfig), "PreStart")]

namespace MediaEinstein.Web.App_Start
{
    public class FilterConfig
    {
        public static void PreStart()
        {
            RegisterGlobalFilters(GlobalFilters.Filters);
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahExceptionFilter());
        }
    }
}