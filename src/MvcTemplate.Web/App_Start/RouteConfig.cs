﻿#region usings

using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MediaEinstein.Web.App_Start;

#endregion
namespace MediaEinstein.Web.App_Start
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "LogOn",
                url: "logon",
                defaults: new { controller = "Authentication", action = "Logon" });

            routes.MapRoute(
                name: "LogIn",
                url: "login",
                defaults: new { controller = "Authentication", action = "Logon" });

            routes.MapRoute(
                name: "LogOff",
                url: "logoff",
                defaults: new { controller = "Authentication", action = "Logoff" });

            routes.MapRoute(
                name: "LogOut",
                url: "logout",
                defaults: new { controller = "Authentication", action = "Logoff" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}