#region usings

using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using MvcTemplate.Domain.Queries;
using MvcTemplate.Web.App_Start;
using StructureMap;
using TrueFit.Web.AutoMapper;
using TrueFit.Web.JsonDotNet;
using TrueFit.Web.StructureMap;
using TrueFit.Web.StructureMap.Validation;
using WebActivator;

#endregion

[assembly: PreApplicationStartMethod(typeof (StructureMapConfig), "PreStart")]

namespace MvcTemplate.Web.App_Start
{
  public static class StructureMapConfig
  {
    public static void PreStart()
    {
      DynamicModuleUtility.RegisterModule(typeof (ReleaseHttpScopedObjectsModule));
      InitializeObjectFactory();
      DependencyResolver.SetResolver(new StructureMapDependencyResolver(ObjectFactory.Container));
    }

    private static void InitializeObjectFactory()
    {
      ObjectFactory.Initialize(
        c =>
          {
            c.Scan(s =>
                     {
                       s.AssemblyContainingType<IQueryExecutor>();
                       s.AssemblyContainingType<JsonDotNetValueProviderFactory>();
                       s.AssemblyContainingType<IMapper>();
                       s.AssemblyContainingType<StructureMapDependencyResolver>();
                       s.TheCallingAssembly();
                       s.AddAllTypesOf(typeof (IModelValidator<>));
                       s.LookForRegistries();
                     });
            c.SetAllProperties(sc => sc.OfType<IContainer>());
          });
    }
  }
}