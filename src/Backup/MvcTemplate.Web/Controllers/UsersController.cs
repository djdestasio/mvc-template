﻿#region usings

using System.Web.Mvc;
using MvcTemplate.Domain.Entities;
using MvcTemplate.Domain.Queries.Users;
using MvcTemplate.Web.Models;
using MvcTemplate.Web.Models.GridBuilders;
using TrueFit.Extensions;
using TrueFit.Web.AutoMapper;

#endregion

namespace MvcTemplate.Web.Controllers
{
  [Authorize]
  public class UsersController : ControllerBase
  {
    private readonly IMapper _mapper;

    public UsersController(IMapper mapper)
    {
      _mapper = mapper;
    }

    [HttpGet]
    public ActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public JsonResult ListData()
    {
      var users = QueryExecutor.ExecuteQuery(new AllUsers());
      var gridData = new UsersGridDataBuilder().BuildGridData(users);
      return Json(gridData);
    }

    [HttpGet]
    public PartialViewResult Add()
    {
      return PartialView("_CreateEditDialog", new UserForm());
    }

    [HttpGet]
    public PartialViewResult Edit(int id)
    {
      var user = NhSession.Get<User>(id);
      var model = _mapper.Map<User, UserForm>(user);
      return PartialView("_CreateEditDialog", model);
    }

    [HttpPost, ValidateAntiForgeryToken]
    public JsonResult Save(UserForm form)
    {
      if (!ModelState.IsValid) return Json(AjaxFormModalResponse.ValidationError(ModelState));

      var user = _mapper.Map<UserForm, User>(form);
      if (form.Id == null) NhSession.Save(user);

      var successMessage = form.Id == null ? Resources.AddedSuccessfully_Formatted : Resources.EditedSuccessfully_Formatted;
      return Json(AjaxFormModalResponse.Success(successMessage.FormatWith(form.Email)));
    }

    [HttpDelete, ValidateAntiForgeryToken]
    public JsonResult Delete(int id)
    {
      var user = NhSession.Get<User>(id);
      if (user == null)
      {
        return Json(AjaxFormModalResponse.Error(Resources.ErrorDeleting_Formatted.FormatWith(string.Empty)));
      }
      NhSession.Delete(user);
      return Json(AjaxFormModalResponse.Success(Resources.DeletedSuccessfully_Formatted.FormatWith(user.Email)));
    }
  }
}