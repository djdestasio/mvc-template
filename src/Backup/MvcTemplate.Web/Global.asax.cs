﻿#region usings

using System.Reflection;
using System.Web;
using System.Web.Mvc;
using TrueFit.Web.HtmlHelpers;

#endregion

namespace MvcTemplate.Web
{
  public class MvcApplication : HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

      Application["Version"] = Assembly.GetExecutingAssembly().GetName().Version.ToString();
      StyleSheetHtmlHelpers.StyleSheetsFolderVirtualPath = Context.IsDebuggingEnabled ? "~/Content/Css/" : "~/Content/Css/minified/" + Application["Version"] + "/";
      ScriptHtmlHelpers.ScriptsFolderVirtualPath = Context.IsDebuggingEnabled ? "~/Scripts/" : "~/Scripts/minified/" + Application["Version"] + "/";
    }
  }
}