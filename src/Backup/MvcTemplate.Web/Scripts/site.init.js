﻿/*jslint indent: 2 */
/*global require */

define(['jquery'], function ($) {
  "use strict";

  $(function () {
    $.ajaxSetup({
      type: 'POST',
      dataType: 'json',
      traditional: true
    });
  });
});