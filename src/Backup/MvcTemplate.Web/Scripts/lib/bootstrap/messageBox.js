﻿/*global define */
// Copyright 2012, TrueFit, Inc.

define(['jquery', 'lib/bootstrap/modal'], function ($) {
  'use strict';

  var confirmHtml,
    alertHtml;

  confirmHtml =
    '<div class="modal">' +
      '<div class="modal-header"><h3></h3></div>' +
      '<div class="modal-body"><div class="alert"></div></div>' +
      '<div class="modal-footer">' +
        '<input type="button" class="btn" value="Cancel" />' +
        '<input type="button" class="btn btn-primary" value="OK" />' +
      '</div>' +
    '</div>';

  alertHtml =
    '<div class="modal">' +
      '<div class="modal-header"><h3></h3></div>' +
      '<div class="modal-body"><div class="alert"></div></div>' +
      '<div class="modal-footer">' +
        '<input type="button" class="btn btn-primary" value="OK" />' +
      '</div>' +
    '</div>';

  function showConfirm(options) {
    var confirmModel = $(confirmHtml)
      .appendTo('body')
      .find('.modal-header h3').text(options.title ? options.title : 'Confirm').end()
      .find('.modal-body .alert').text(options.text).end();

    confirmModel.on('click', '.modal-footer input[type="button"][value="Cancel"]', function () {
      if ($.isFunction(options.cancel)) { options.cancel(); }
      confirmModel.modal('hide');
    });

    confirmModel.on('click', '.modal-footer input[type="button"][value="OK"]', function () {
      if ($.isFunction(options.ok)) { options.ok(); }
      confirmModel.modal('hide');
    });

    confirmModel.on('hidden', function () {
      setTimeout(function () {
        confirmModel.remove();
      });
    });

    confirmModel.modal({ keyboard: false, backdrop: 'static' });
  }

  function showAlert(options) {
    var alertModal = $(alertHtml)
      .appendTo('body')
      .find('.modal-header h3').text(options.title ? options.title : 'Alert').end()
      .find('.modal-body .alert').text(options.text).addClass(options.type ? 'alert-' + options.type : '').end();

    alertModal.on('click', '.modal-footer input[type="button"][value="OK"]', function () {
      if ($.isFunction(options.ok)) { options.ok(); }
      alertModal.modal('hide');
    });

    alertModal.on('hidden', function () {
      setTimeout(function () {
        alertModal.remove();
      });
    });

    alertModal.modal({ keyboard: false, backdrop: 'static' });
  }

  return {
    alert: function (options) {
      if (typeof options === 'string') {
        options = {
          title: 'Alert',
          text: options
        };
      }
      showAlert(options);
    },

    confirm: function (options) {
      showConfirm(options);
    }
  };
});