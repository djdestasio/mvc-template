#region usings

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MvcTemplate.Web.Infrastructure.Validation;

#endregion

namespace MvcTemplate.Web.Models
{
  public class UserForm
  {
    public int? Id { get; set; }

    [Required, StringLength(256)]
    public string Email { get; set; }

    [ValidatePasswordLength]
    public string Password { get; set; }

    [Display(Name = "Confirm Password")]
    [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
    public string ConfirmPassword { get; set; }
  }
}