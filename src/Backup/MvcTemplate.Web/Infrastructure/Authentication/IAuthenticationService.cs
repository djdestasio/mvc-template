﻿namespace MvcTemplate.Web.Infrastructure.Authentication
{
  public interface IAuthenticationService
  {
    bool LogOn(string email, string password);
    void LogOff();
  }
}