#region usings

using System;
using System.Diagnostics;
using System.Security.Cryptography;

#endregion

namespace MvcTemplate.Web.Infrastructure.Authentication
{
  public class PasswordHasher
  {
    private const int SaltLength = 16;
    private const int PasswordLength = 32;
    private const int HashIterations = 4000;

    public string HashPassword(string salt, string password)
    {
      var saltBytes = Convert.FromBase64String(salt);
      using (var hasher = new Rfc2898DeriveBytes(password, saltBytes, HashIterations))
      {
        var hash = hasher.GetBytes(PasswordLength);
        return Convert.ToBase64String(hash);
      }
    }

    public string GeneratePasswordSalt()
    {
      using (var random = RandomNumberGenerator.Create())
      {
        var salt = new byte[SaltLength];
        random.GetNonZeroBytes(salt);
        return Convert.ToBase64String(salt);
      }
    }
  }
}