﻿#region usings

using MvcTemplate.Domain.Orm;
using NHibernate;
using StructureMap.Configuration.DSL;

#endregion

namespace MvcTemplate.Web.Infrastructure.Orm.IoC
{
  public class OrmRegistry : Registry
  {
    public OrmRegistry()
    {
      For<ISession>().HttpContextScoped().Use(context => context.GetInstance<ISessionFactory>().OpenSession());
      SetAllProperties(sc => sc.OfType<ISession>());

      For<IConnectionStringProvider>().Use<ConnectionStringProvider>();
    }
  }
}